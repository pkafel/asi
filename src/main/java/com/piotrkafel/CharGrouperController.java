package com.piotrkafel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
@ComponentScan("com.piotrkafel")
public class CharGrouperController {

    @Autowired
    private CharGrouper charGrouper;

    @RequestMapping(value = "/group", method = RequestMethod.POST)
    public String group(@RequestBody String input) {
        return charGrouper.group(input);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CharGrouperController.class, args);
    }
}
