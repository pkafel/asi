package com.piotrkafel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CharGrouper {

    @Autowired
    private ArraySorter arraySorter;

    @Autowired
    private StringCompressor stringCompressor;

    public String group(String input) {
        int[] sorted = arraySorter.sort(input.codePoints().toArray());
        return stringCompressor.compress(new String(sorted, 0, sorted.length));
    }
}
