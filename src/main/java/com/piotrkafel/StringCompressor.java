package com.piotrkafel;

import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Stream.generate;

/**
 * I had some concerns about this task because the description
 * "Implement method compress so that the tests are green"
 * can be interpreted in so many ways.
 *
 * The simples implementation for this task would to just include three ifs inside method so they are returning correct
 * results only for those three tests from the task. This is from where I started. Then I refactored the algorithm to
 * what you can see. In the last step I added the first line which is checking length even that there is no test for it.
 */
@Service
public class StringCompressor {

    public String compress(String uncompressed) {
        if(uncompressed.length() == 1) return uncompressed;

        StringBuilder result = new StringBuilder();

        char currentChar = uncompressed.charAt(0);
        int count = 1;
        for(int i = 1;i < uncompressed.length();i++) {
            if(uncompressed.charAt(i) == currentChar) {
                count++;
            } else {
                result.append(getRepresentation(currentChar, count));
                currentChar = uncompressed.charAt(i);
                count = 1;
            }
        }

        return result.append(getRepresentation(currentChar, count)).toString();
    }

    private String getRepresentation(char c, int count) {
        String character = Character.toString(c);

        if(c == 'l') return generate(() -> character).limit(count).collect(joining());
        if(count == 1) return character;

        return character + count;
    }
}
