package com.piotrkafel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CharGrouper.class, ArraySorter.class, StringCompressor.class})
public class CharGrouperTest {

    @Autowired
    private CharGrouper charGrouper;

    @Test
    public void shouldReturnCorrectlyGroupedString() {
        assertEquals("a4bins2uz", charGrouper.group("abzuaaissna"));
    }
}
