package com.piotrkafel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CharGrouperController.class)
@WebIntegrationTest
public class CharGrouperControllerTest {

    private static final String url = "http://localhost:8080/group";

    private final RestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void shouldReturnProperOutput() {
        // Given
        String input = "abzuaaissna";

        // When
        ResponseEntity<String> response = restTemplate.postForEntity(url, input, String.class);

        // Then
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals("a4bins2uz", response.getBody());
    }
}
