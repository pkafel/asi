package com.piotrkafel;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class BonusBTest {

    @Test
    public void testChangeFooBar() throws NoSuchFieldException, IllegalAccessException {

        TestClass testClass = new TestClass();
        assertEquals("test", testClass.getFoobar());

        Field fooBarField = testClass.getClass().getDeclaredField("foobar");
        fooBarField.setAccessible(true);
        fooBarField.set(testClass, "SUCCESS");

        assertEquals("SUCCESS", testClass.getFoobar());
    }
}

class TestClass {

    private String foobar = "test";

    public String getFoobar() {
        return foobar;
    }
}
