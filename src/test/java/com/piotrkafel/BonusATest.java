package com.piotrkafel;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BonusATest {

    @Test
    public void test() {
        String s = "1";
        Integer i = 1;

        s = add(s, 1);
        assertEquals("2", s);
        s = add(s, 5);
        assertEquals("7", s);

        i = add(i, 2);
        assertEquals((Integer) 3, i);
        i = add(i, 1);
        assertEquals((Integer) 4, i);
    }

    private <T, W> T add(T t, W w) {
        Integer result = Integer.valueOf(t.toString()) + Integer.valueOf(w.toString());
        if(t instanceof String) return (T)result.toString();
        else return (T)result;
    }
}
