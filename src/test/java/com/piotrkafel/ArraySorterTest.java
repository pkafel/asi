package com.piotrkafel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test class for ArraySorter.
 *
 * One thing worth noting is history of a bug in array sort implementation mentioned by Josh Bloch in his "Effective Java".
 * The story can be found here:
 *
 * @see <a href="http://googleresearch.blogspot.de/2006/06/extra-extra-read-all-about-it-nearly.html">Sort implementation bug</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ArraySorter.class)
public class ArraySorterTest {

    @Autowired
    private ArraySorter arraySorter;

    /**
     * Checking corner cases like passing null value.
     */
    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenPassedNull() {
        arraySorter.sort(null);
    }

    /**
     * Checking behavior for passing null value.
     */
    @Test
    public void shouldReturnEmptyArrayForEmptyInputArray() {
        // Given
        int[] input = new int[0];

        // When
        int[] result = arraySorter.sort(input);

        // Then
        Assert.assertNotNull("Result of sorting empty array should not be null", result);
        Assert.assertArrayEquals("Result of sorting empty array should be empty array", new int[0], result);
    }

    /**
     * This test checks two things. If sorting of one element array is returning *new* array and if the result array
     * contains exactly the same element as in the original array.
     */
    @Test
    public void shouldReturnArrayWithTheSameSingleValueForOneElementInputArray() {
        // Given
        int[] input = new int[] { 44 };

        // When
        int[] result = arraySorter.sort(input);

        // Then
        Assert.assertArrayEquals("Result of sorting one element array should return one element array", input, result);
        Assert.assertTrue("Sort method should produce new array and not modify input one", input != result);
    }

    /**
     * Regular test that checks if method really sorts array of ints. One can argue that Im testing on single example
     * and I should have created a generator in order to test many different arrays (which in Java 8 is actually super easy to do:
     * <pre>
     *     {@code new Random().ints(10).toArray(); }
     * </pre>
     * ) but then we need to ensure that the generated array is really unsorted which introduce logic that we need to
     * test again. Testing test util for such an easy example sounds a bit like overengineering...
     */
    @Test
    public void shouldReturnSortedArrayForUnsortedArrayInput() {
        // Given
        int[] input = new int[]{1,6,4,2,6,9,55,78,2,54,3};

        // When
        int[] result = arraySorter.sort(input);

        // Then
        Assert.assertArrayEquals(new int[] {1,2,2,3,4,6,6,9,54,55,78}, result);
    }
}
