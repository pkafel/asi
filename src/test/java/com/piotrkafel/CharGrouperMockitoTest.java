package com.piotrkafel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.mockito.Matchers.argThat;

@RunWith(MockitoJUnitRunner.class)
public class CharGrouperMockitoTest {

    @Mock
    private ArraySorter arraySorter;

    @Spy
    private StringCompressor stringCompressor;

    @Spy
    @InjectMocks
    private CharGrouper charGrouper;

    @Before
    public void before() {
        Mockito.doReturn("").when(charGrouper).group(argThat(is((not("abcdef")))));
        Mockito.doReturn("SUCCESS").when(charGrouper).group(argThat(is("abcdef")));
    }

    @Test
    public void test1() {
        Assert.assertEquals("", charGrouper.group("test"));
        Mockito.verify(charGrouper).group("test");
    }

    @Test
    public void test2() {
        Assert.assertEquals("SUCCESS", charGrouper.group("abcdef"));
        Mockito.verify(charGrouper).group("abcdef");
    }
}
